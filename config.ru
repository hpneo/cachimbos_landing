run lambda { |env|
  path = env["PATH_INFO"]

  response = []
  content_type = 'text/html'

  if path == '/'
    response = File.open('landing.html', File::RDONLY)
  else
    response = File.open(path.split('/').pop, File::RDONLY)

    if path.split('/').pop.include?('.jpeg')
      content_type = 'image/jpg'
    end

    if path.split('/').pop.include?('.png')
      content_type = 'image/png'
    end
  end

  [
    200,
    {
      'Content-Type'  => content_type,
      'Cache-Control' => 'public, max-age=86400'
    },
    response
  ]
}